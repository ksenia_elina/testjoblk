class Changestring < ActiveRecord::Migration[5.2]
  def change
    change_column(:avatars,:body, :string, limit: 1000)
  end
end
