class Change < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :avatar_url, :string
    add_reference :users, :avatars, foreign_key: true
  end
end
