class ChangeUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users :avatar_url :string
    change_table :users do |t|
      t.references :avatars, index: true, foreign_key: true
    end
  end
end
