class ChangeAvatar < ActiveRecord::Migration[5.2]
  def change
    add_column :avatars, :body, :string
  end
end
