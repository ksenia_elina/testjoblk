class Avatar < ApplicationRecord
    mount_uploader :body, AvatarUploader

    validates :body, file_content_type: { allow: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif'] }
end
